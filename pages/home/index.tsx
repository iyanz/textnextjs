import Nav from "../../component/header";
import Head from "next/head";

export default function index({singlePost, imagePost}) {     
    
    //console.log(singlePost);
    return (
        <>
            <Head>
                <title>Posts</title>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossOrigin="anonymous"></link>                
            </Head>
        <Nav>

        </Nav>
        <div className="container">
                {singlePost.slice(0,10).map((posts) => {
                return (<> 
                <div key={posts.id} className="card" style={{width: 18 + 'rem', display : 'inline-block', marginRight: 10 + 'px',  marginBottom: 10 + 'px'}}>
                {imagePost.slice(0,10).map(images => {
                        return( images.id == posts.id ? (
                                <>
                                <img key={images.id} src={images.thumbnailUrl} className="card-img-top" alt={images.title}/>
                                <div className="card-body">
                                    <h5 className="card-title">{posts.title}</h5>
                                    <p className="card-text">{posts.body}</p>
                                    <a href={images.url} className="btn btn-primary">Go somewhere</a>
                                </div>
                                </>
                            ) : (
                                <>

                                </>
                            )
                        )
                    })}  
                </div>
                </>)      
                })}
        </div>
        
        </>
    )
}

export async function getServerSideProps() {
    const settings = {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json',
        }
    }

    //
    const res = await fetch('https://jsonplaceholder.typicode.com/posts', settings);
    const singlePost = await res.json();
    
    //Image API
    const img = await fetch("https://jsonplaceholder.typicode.com/photos",settings);
    const imagePost = await img.json();
    return {
      props: {
        singlePost,
        imagePost
      }, 
    }
  }

